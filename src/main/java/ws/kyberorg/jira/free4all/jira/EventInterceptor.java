package ws.kyberorg.jira.free4all.jira;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import ws.kyberorg.jira.free4all.handler.CommentAdded;
import ws.kyberorg.jira.free4all.tech.Logger;

/**
 * Intercepts events
 */
public class EventInterceptor implements InitializingBean, DisposableBean {

    private static final Logger log = Logger.getLogger("EventInterceptor");

    private final EventPublisher eventPublisher;

    /**
     * Constructor need for autowire
     *
     * @param eventPublisher registrar
     */
    public EventInterceptor(EventPublisher eventPublisher){
        this.eventPublisher = eventPublisher;
    }

    /**
     * After plugin started/re-deployed
     *
     * @throws Exception
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Registering Sputnik Interceptor");
        this.eventPublisher.register(this);
    }

    /**
     * Before plugin unloaded/un-deployed/stopped
     *
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        log.info("UnRegistering Sputnik Interceptor");
        this.eventPublisher.unregister(this);
    }

    /**
     * Intercepts event for us
     *
     * @param event Jira Event
     */
    @EventListener
    public void intercept(IssueEvent event){
        Long eventTypeId = event.getEventTypeId();

        try{
            if(eventTypeId.equals(EventType.ISSUE_COMMENTED_ID)){
                CommentAdded.update(event);
            }
        }catch (Exception e){
            log.error(e.getMessage());
            e.printStackTrace(System.err);
        }



    }




}
