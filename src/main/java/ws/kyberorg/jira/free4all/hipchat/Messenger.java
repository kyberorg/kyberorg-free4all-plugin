package ws.kyberorg.jira.free4all.hipchat;

import ws.kyberorg.jira.free4all.tech.Logger;
import ws.kyberorg.jira.free4all.tech.request.Response;

/**
 * Sends messages to HipChat
 */
public class Messenger {
    private static Logger log = Logger.getLogger("Messenger");

    public static void send(String hipchatUser,String message){
        Response rslt = HipChatClient.message2User(hipchatUser,message).exec(); //run()

        if(rslt.status()== Response.Status.OK){
            switch (rslt.getStatusCode()){
                case 204:
                    log.info("Successfully proceed");
                    //TODO messagePF counter
                    break;
                case 401:
                    log.error("HipChat says that we are unauthorized to send messages");
                    break;
                case 404:
                    log.error("No such user in hipchat");
                    break;
                default:
                    log.error("Unknown yet Error occurred. HttpStatus: "+rslt.getCode());
                    break;
            }
        } else {
            log.error("HipChat unreachable");
            log.debug("Exception occurred when contacting to HipChat: "
                    +rslt.showException().getMessage());
            log.debug("Object state: "+rslt.toString());
        }
    }

}