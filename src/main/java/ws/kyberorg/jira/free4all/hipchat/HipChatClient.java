package ws.kyberorg.jira.free4all.hipchat;

import com.google.gson.Gson;
import ws.kyberorg.jira.free4all.Settings;
import ws.kyberorg.jira.free4all.tech.Logger;
import ws.kyberorg.jira.free4all.tech.request.Helper;
import ws.kyberorg.jira.free4all.tech.request.HttpMethod;
import ws.kyberorg.jira.free4all.tech.request.Response;
import ws.kyberorg.jira.free4all.tech.request.SputnikHttpClient;

/**
 * Class that works with hipchat
 */
public class HipChatClient {
    private static Logger log = Logger.getLogger("HipChat Client");

    private static HipChatClient self = new HipChatClient();

    private HttpMethod method;
    private StringBuilder URL = new StringBuilder();
    private String postPayload = "";


    private HipChatClient(){}

    private void init(){
        this.URL.append(Settings.HipChatURL).append("/");
        this.URL.append(Settings.HipChatAPIVersion).append("/");
    }

    /**
     * Sends messagePF to individual user
     *
     * @param userEmail HipChat Email
     * @return HipChatClient object
     */
    public static HipChatClient message2User(String userEmail,String message){
        self.init();

        self.method = HttpMethod.POST;

        self.URL.append("user").append("/");
        self.URL.append(userEmail).append("/");
        self.URL.append("message");

        self.postPayload = message;

        return self;
    }

    public Response exec(){

        //append token
        this.URL.append("?").append("auth_token=").append(Settings.HipChatAuthToken);

        //create
        SputnikHttpClient httpClient;
        httpClient = SputnikHttpClient.method(this.method).url(this.URL.toString());

        httpClient.headers(Helper.getDefaultPostHeaders());
        //httpClient.header(new BasicHeader("accept", "application/json"));
        //httpClient.header(new BasicHeader("Content-Type","application/json"));

        //if post and payload present add it
        if(this.method==HttpMethod.POST && ! this.postPayload.isEmpty()){
            //From text to Json
            Gson gson = new Gson();
            Message msg = new Message(this.postPayload);
            this.postPayload = gson.toJson(msg);


            httpClient.payload(this.postPayload);
        }
        //exec
        Response rslt = httpClient.exec();

        //clean up
        self.URL = new StringBuilder();

        return rslt;
    }

    /**
     * Alias to exec()
     */
    public void run(){ exec();}
}
