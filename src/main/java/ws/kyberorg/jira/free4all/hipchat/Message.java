package ws.kyberorg.jira.free4all.hipchat;

/**
 * Boxes messagePF for Json
 */
public class Message {
    public String message;

    public Message(String message){
        this.message = message;
    }
}
