package ws.kyberorg.jira.free4all.workflow;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;

import java.util.Map;

/**
 * Edit this.
 */
public class Message2AssigneeFactory extends AbstractWorkflowPluginFactory implements
        WorkflowPluginFunctionFactory {
    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        //velocityParams.put("Void","Void");
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        //velocityParams.put("Some","Thing");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        //velocityParams.put("Some","Thing");
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        return EasyMap.build();
    }
}
