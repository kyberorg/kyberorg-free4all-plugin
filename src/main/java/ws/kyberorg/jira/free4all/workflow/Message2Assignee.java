package ws.kyberorg.jira.free4all.workflow;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import ws.kyberorg.jira.free4all.tech.Logger;

import java.util.Map;

/**
 * Post Function
 */
public class Message2Assignee extends AbstractJiraFunctionProvider {

    private static final Logger log = Logger.getLogger("Message to Assignee Post Function");

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);
        log.debug("Issue "+issue.getKey()+" updated");
    }
}
