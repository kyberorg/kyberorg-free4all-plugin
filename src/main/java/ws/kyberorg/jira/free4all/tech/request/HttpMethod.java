package ws.kyberorg.jira.free4all.tech.request;

/**
 * Supported Http Method
 */
public enum HttpMethod {
    GET,
    POST,
    PUT,
    DELETE
}
