package ws.kyberorg.jira.free4all.handler;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comments.Comment;
import ws.kyberorg.jira.free4all.hipchat.Messenger;
import ws.kyberorg.jira.free4all.tech.Logger;

/**
 * Notifies HipChat
 */
public class CommentAdded {
    private static Logger log = Logger.getLogger("onCommentAdded");
    public static void update(IssueEvent event){

        //jira user
        User user = event.getUser();
        String userEmail = user.getEmailAddress();
        log.debug("user email is: "+userEmail);

        //messagePF prepare
        Comment comment = event.getComment();
        Issue issue = event.getIssue();

        StringBuilder msg = new StringBuilder();
        msg.append("New Comment: ").append("'");
        msg.append(comment.getBody()).append("'");
        msg.append(" added by ").append(comment.getAuthorFullName());
        msg.append(" to issue ").append(issue.getKey());
        msg.append(" at ").append(comment.getUpdated());

        //act
        Messenger.send(userEmail,msg.toString());

    }
}
